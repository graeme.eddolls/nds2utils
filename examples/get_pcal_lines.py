"""Get and plot the PCAL lines and DARM together

Craig and Francisco
"""

import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:CAL-PCALY_RX_PD_OUT_DQ"]

gps_start = 1344010515 

averages = 30 
binwidth = 0.1 # Hz
overlap = 0.0

duration = int(nu.dtt_time(averages, binwidth, overlap))

gps_stop = gps_start + duration

server = "nds.ligo-wa.caltech.edu"
port = 31200

data_dict = nu.get_psds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=server,
    port_number=port,
)

# How to save a convenient hdf5 file for quick saving/loading
# nu.save_hdf5(data_dict, "test_dict.hdf5")

# Make the DARM calibration using zpk
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
data_dict = nu.calibrate_chan(
    data_dict,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,
)

# Calibrate the PCALY
zeros_pcal = []
poles_pcal = [1.0, 1.0]
gain_pcal = 1.0
units_pcal = "m"
data_dict = nu.calibrate_chan(
    data_dict,
    "H1:CAL-PCALY_RX_PD_OUT_DQ",
    zeros=zeros_pcal,
    poles=poles_pcal,
    gain=gain_pcal,
    units=units_pcal,
)

# make the plot
xlims = [7, 7000]
ylims = [1e-20, 1e-15]

import matplotlib.pyplot as plt

nu.plot_asds(
    data_dict,
    title="DARM with the PCALY lines",
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.show()