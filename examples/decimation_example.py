"""decimation_example.py

Does a decimation of a signal via nds2utils,
for transfer function computation.
"""
# import os
import numpy as np
import scipy.signal as sig

import nds2utils as nu

### FFT Parameters
averages = 100

nperseg = 2**14  # number of samples in a single fft segment
noverlap = 0  # nperseg // 2      # 0% overlap

nn = (averages + 1) * nperseg  # number of samples
fs = 2**14  # Hz, sampling frequency, samples/second
f_nyq = fs / 2  # Hz, Nyquist frequency
total_time = nn / fs  # seconds, total time

bandwidth = fs / nperseg  # width of the PSD frequency bins
overlap = noverlap / nperseg

# averages = (total_time * bandwidth - 1)/(1 - overlap) + 1

print("\033[92m")  # turns terminal text green
print(f"CSD Parameters")
print(f"total samples nn = {nn}")
print(f"sampling frequency = {fs} Hz")
print()
print(f"total_time = {total_time} seconds")
print(f"bandwidth = {bandwidth} Hz")
print(f"overlap = {100 * overlap} %")
print(f"averages = {averages}")
print("\033[0m")  # turns terminal text back to normal


### Noise power
# power density in V**2/Hz.  Should show up in the power spectral density.
noise_power_density = 1e-3

# total power in the noise spectrum in V**2.
# Equal to the variance of the gaussian noise.
noise_power = noise_power_density * fs / 2

# Gaussian noise standard deviation
noise_amplitude = np.sqrt(noise_power)


### Signals
# Get sample times
tt = np.linspace(0, total_time, nn)

xx = np.random.normal(scale=noise_amplitude, size=nn)


### Transfer functions
# zpk in Hz
zeros_Hz = np.array([100.0, 400.0, 1000.0, 2000.0, 4000.0])  # Hz
poles_Hz = np.array([5.0, 10.0, 20.0, 40.0, 60.0])  # Hz
gain_Hz = 1.0

# zpk in rad/s
zeros_rads = -2 * np.pi * zeros_Hz  # rad/s
poles_rads = -2 * np.pi * poles_Hz  # rad/s
gain_rads = gain_Hz * np.abs(np.prod(poles_rads) / np.prod(zeros_rads))

# zpk in units of z-plane normalized frequency
zeros_n, poles_n, gain_n = sig.bilinear_zpk(zeros_rads, poles_rads, gain_rads, fs)


print("\033[93m")
print(f"zeros_n = {zeros_n}")
print(f"poles_n = {poles_n}")
print(f"gain_n = {gain_n}")
print("\033[0m")


# Apply zpk filter to xx data to produce filtered yy data
sos = sig.zpk2sos(zeros_n, poles_n, gain_n)

zi = sig.sosfilt_zi(sos)
yy, _ = sig.sosfilt(sos, xx, zi=zi * xx[0])

print(sos)

### Decimate
downsampling_factor = 2
fs_new = fs // downsampling_factor
nperseg_new = nperseg // downsampling_factor

zz = sig.decimate(yy, downsampling_factor)

# Produce the spectral densities
ff, Sxx = sig.welch(xx, fs, nperseg=nperseg, noverlap=noverlap)
_, Syy = sig.welch(yy, fs, nperseg=nperseg, noverlap=noverlap)
ff_new, Szz = sig.welch(zz, fs_new, nperseg=nperseg_new, noverlap=noverlap)

### Frequency response of the transfer function
ff3 = np.logspace(np.log10(ff[1]), np.log10(ff[-1]), 1000)

## Digital
w3 = 2 * np.pi * ff3 / fs  # normalized freq on domain [0, pi]
_, h3 = sig.sosfreqz(sos, worN=w3)

## Analog
ww = 2 * np.pi * ff3  # rad/s
_, hh = sig.freqs_zpk(zeros_rads, poles_rads, gain_rads, worN=ww)


### Construct a fake data_dict for use by nds2utils
data_dict = {
    "xx": {
        "data": xx,
        "fs": fs,
        "duration": total_time,
    },
    "yy": {
        "data": yy,
        "fs": fs,
        "duration": total_time,
    },
    "zz": {
        "data": zz,
        "fs": fs_new,
        "duration": total_time,
    }
}

# Calculate the PSDs, CDSs, and TF from xx to zz
data_dict = nu.calc_csds(data_dict, averages, bandwidth, overlap)

ff_hxz = data_dict["zz"]["xx"]["ff"]
tf_hxz = data_dict["zz"]["xx"]["TF"]

### Figures
import matplotlib.pyplot as plt

# Plot the spectral densities
fig, (s1, s2) = plt.subplots(2, sharex=True)


(pxy,) = s1.loglog(ff_hxz, np.abs(tf_hxz), label="$|H_{xz}|$")
(ph3,) = s1.loglog(ff3, noise_power_density * np.abs(h3), label="$|H_{digital}|$")
s1.loglog(ff3, noise_power_density * np.abs(h3) ** 2, label="$|H_{digital}|^2$")
(phh,) = s1.loglog(ff3, noise_power_density * np.abs(hh), label="$|H_{analog}|$")

s2.semilogx(ff, np.angle(Sxz, deg=True), color=pxy.get_color(), label="$\angle S_{xy}$")
s2.semilogx(
    ff3, np.angle(h3, deg=True), color=ph3.get_color(), label="$\angle H_{digital}$"
)
s2.semilogx(
    ff3, np.angle(hh, deg=True), color=phh.get_color(), label="$\angle H_{analog}$"
)

s1.set_ylim([1e-6, 1e-2])
s2.set_ylim([-90, 0])
s2.set_yticks(np.arange(-2, 1) * 45)

s1.grid(which="minor", ls="--", alpha=0.5)
s2.grid(which="minor", ls="--", alpha=0.5)

s1.legend()

s1.set_title("PSD comparison")
s1.set_ylabel("Power spectral density [$\mathrm{V}^2/\mathrm{Hz}$]")
s2.set_ylabel("Phase [deg]")
s2.set_xlabel("Frequency [Hz]")

plt.show()

# fig_name = "decimation_example.pdf"
# full_fig_name = os.path.join(fig_dir, fig_name)
# plt.tight_layout()
# plt.savefig(full_fig_name)
# print("Figure saved to:")
# print(full_fig_name)
