"""
make_ASDs_svg.py
Creates an interactive .svg plot file from a typical nds2utils.plot_ASDs() plot.

Craig Cahillane
Nov 23, 2019
"""

import matplotlib.pyplot as plt
import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg

plt.ion()
# need to do this so that plt.show() does not halt the script,
# forcing us to close the fig, which dumps the lines detected in the xml tree

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start_bad = 1256771396
gps_stop_bad = 1256771546
gps_start_good = 1256771562
gps_stop_good = 1256771712

binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict_bad = nu.get_psds(
    channels,
    gps_start_bad,
    gps_stop_bad,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)
dataDict_good = nu.get_psds(
    channels,
    gps_start_good,
    gps_stop_good,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

fig = nu.compare_asds(
    dataDict_bad,
    dataDict_good,
    label1="Bad",
    label2="Good",
    title="Uncalibrated and Logbinned DARM and ISS spectra",
    logbin=True,
)

make_interactive_svg(fig, filename="make_asds_svg")
