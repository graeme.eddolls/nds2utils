'''
make_subplots_svg.py
Creates an interactive .svg plot file for a group of subplots with one legend.
Will toggle lines in all subplots if line anywhere is clicked.

Craig Cahillane
Nov 23, 2019
'''

import numpy as np
import matplotlib.pyplot as plt
from nds2utils.make_interactive_svg import make_interactive_svg_multiple_subplots

xx = np.linspace(0.0, 1.0, 50)
yy = xx**(-xx)
yy2 = 0.1*xx**(2)+1
yy3 = 0.3*(xx-0.5)**(3)+1

fig, ss = plt.subplots(2)
s1 = ss[0]
s2 = ss[1]

s1.plot(xx, yy, lw=3, label='yy')
s1.plot(xx, yy2, lw=3, label='yy2')
s1.plot(xx, yy3, lw=3, label='yy3')

s2.plot(xx, -yy, lw=3, ls='--', alpha=0.5, label='-yy')
s2.plot(xx, -yy2, lw=3, ls='--', alpha=0.5, label='-yy2')
s2.plot(xx, -yy3, lw=3, ls='--', alpha=0.5, label='-yy3')

s1.legend()
s2.legend()

make_interactive_svg_multiple_subplots(fig, 'make_subplots_svg')
