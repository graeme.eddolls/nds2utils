"""
Plots the raw TF and coherence between DARM and the ISS at a time with high coherence.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start = 1256771396
gps_stop = 1256771546

binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.get_csds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

import matplotlib.pyplot as plt

nu.plot_tf(
    dataDict,
    "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ",
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    title="Raw DARM/ISS TF at time in O3b with high coherence",
    logbin=True,
    label="Raw DARM/ISS",
)
plt.show()
