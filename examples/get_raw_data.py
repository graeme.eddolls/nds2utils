"""
Gets raw H1:CAL-DELTAL_EXTERNAL_DQ data time series.
Default host_server:port_number = nds.ligo-wa.caltech.edu:31200

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu
import gpstime

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:LSC-REFL_SERVO_ERR_OUT_DQ"]
# gps_start = 1256771562
# gps_stop = 1256771712
gps_start = gpstime.gpsnow() - 300
gps_stop = gpstime.gpsnow() - 270

host_server = "nds.ligo-wa.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

print(f"gps_start = {gps_start}")
print(f"gps_stop = {gps_stop}")

data_dict = nu.acquire_data(
    channels,
    gps_start,
    gps_stop,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

# this is where the data is stored
cal_deltal_external_data = data_dict["H1:CAL-DELTAL_EXTERNAL_DQ"]["data"]

# Plot raw time series data
import matplotlib.pyplot as plt

nu.plot_raw_data(data_dict, seconds=150, downsample=2**10)
plt.show()
