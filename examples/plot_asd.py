"""
Gets H1:CAL-DELTAL_EXTERNAL_DQ data time series, and plots the raw ASDs.

Craig Cahillane
Nov 5, 2019
"""

import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start = 1256771562
gps_stop = 1256771712
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

hostname = "nds.ligo.caltech.edu"
port = 31200
allow_data_on_tape = "True"

dataDict = nu.get_psds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=hostname,
    port_number=port,
    allow_data_on_tape=allow_data_on_tape,
)

import matplotlib.pyplot as plt

fig = nu.plot_asds(
    dataDict, title="Uncalibrated and Logbinned DARM and ISS spectra", logbin=True
)
plt.show()
