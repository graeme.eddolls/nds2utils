"""
Plots the raw TF and coherence
between the ISS and several IFO ports at the time of an injection.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = [
    "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ",  # secondloop witness
    "H1:PSL-ISS_PDA_REL_OUT_DQ",
    "H1:PSL-ISS_PDB_REL_OUT_DQ",  # firstloop witnesses
    "H1:LSC-REFL_A_LF_OUT_DQ",
    "H1:LSC-REFL_B_LF_OUT_DQ",  # refl
    "H1:LSC-POP_A_LF_OUT_DQ",  # pop
    "H1:OMC-DCPD_SUM_OUT_DQ",  # as port
    "H1:ASC-X_TR_A_NSUM_OUT_DQ",
    "H1:ASC-X_TR_B_NSUM_OUT_DQ",  # xarm
    "H1:ASC-Y_TR_A_NSUM_OUT_DQ",
    "H1:ASC-Y_TR_B_NSUM_OUT_DQ",  # yarm
]
gps_start = 1256622575
duration = 80  # s
gps_stop = gps_start + duration
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.get_csds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

# nu.plot_TFs_A always assumes the channel given is the A channel, and all other channels are the B channels.
import matplotlib.pyplot as plt

nu.plot_tfs_a(
    dataDict,
    "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ",
    title="Raw chan/ISS TF at the time of an intensity injection\n [legend]/{}".format(
        channels[0]
    ),
    logbin=True,
    num_figs=3,
)
plt.show()
