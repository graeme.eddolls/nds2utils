'''
csd_bias_correction.py

Generates some example white noise data, 
calculates the mean- and median-averaged cross spectral densities,
and calculates the mean-to-median bias correction for the median-averaged CSD
from the median coherence.

Craig Cahillane
July 13, 2020
'''

import nds2utils as nu
import numpy as np

#####   Time Series Parameters   #####
avg = 1000            # Number of CSDs
nperseg = 1000        # number of samples in a single fft segment
N = avg * nperseg     # total number of samples
fs = 1e4              # Hz, sampling frequency, samples/second
total_time = N / fs   # seconds, total time

noverlap = 0          # 0% overlap

bandwidth = fs / nperseg
overlap = noverlap / nperseg

print()
print(f'total samples N = {N}')
print(f'sampling frequency = {fs} Hz')
print()
print(f'total_time = {total_time} seconds')
print(f'bandwidth = {bandwidth} Hz')
print(f'overlap = {100 * overlap} %')
print(f'averages = {avg}')

noise_power_density = 1e-3      # power density in V**2/Hz.
corr_noise_power_density = 7e-4 # correlated noise power density in V**2/Hz.  Should be the limit the CSD hits
total_noise_power_density = noise_power_density + corr_noise_power_density  # Should be the limit the PSD hits

noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                        # Equal to the variance of the gaussian noise.  
corr_noise_power = corr_noise_power_density * fs / 2

# Make times domain signals
a = np.random.normal(scale=np.sqrt(noise_power), size=N) 
b = np.random.normal(scale=np.sqrt(noise_power), size=N)
c = np.random.normal(scale=np.sqrt(corr_noise_power), size=N)

x = a + c
y = b + c 

# Return all PSDs and CSDs directly
ff, tt, Sxxs = nu.get_all_psds(x, fs, nperseg=nperseg, noverlap=noverlap)
_, _, Syys   = nu.get_all_psds(y, fs, nperseg=nperseg, noverlap=noverlap)

_, _, Sxys   = nu.get_all_csds(x, y, fs, nperseg=nperseg, noverlap=noverlap)

Sxx_mean = nu.get_mean_psd(Sxxs)
Syy_mean = nu.get_mean_psd(Syys)
Sxy_mean = nu.get_mean_csd(Sxys)

Sxx_med = nu.get_median_psd(Sxxs)
Syy_med = nu.get_median_psd(Syys)
Sxy_med = nu.get_median_csd(Sxys)

cohs_mean = nu.get_coherence(Sxx_mean, Syy_mean, Sxy_mean)

cohs_med = nu.get_coherence(Sxx_med, Syy_med, Sxy_med)
biases = nu.biases_from_median_coherences(cohs_med)

Sxy_med_bias_corrected = np.real(Sxy_med) / biases

mean_cohs_med = np.mean(cohs_med[1:-1])
mean_bias = np.mean(biases[1:-1])

print(f'Median coherence    = {mean_cohs_med}')
print(f'Mean-to-median bias = {mean_bias}')

#####   Figures   #####
import matplotlib.pyplot as plt
# plt.ion()

fig, s1 = plt.subplots(1) 

s1.semilogy(ff, np.real(Sxy_mean), 'C1-', label='Mean $\Re(\mathrm{CSD})$ estimate')
s1.semilogy(ff, np.real(Sxy_med), 'C6-', label='Median $\Re(\mathrm{CSD})$ estimate (not bias-corrected)')
s1.semilogy(ff, Sxy_med_bias_corrected, 'C0-', label='Median $\Re(\mathrm{CSD})$ estimate (bias-corrected)')
s1.semilogy(ff, corr_noise_power_density * np.ones_like(ff), 'C3--', label='Ideal $\Re(\mathrm{CSD})$')

s1.set_title("Cross-spectral density averaging" + '\n' 
            + r'Averages M = ' + f'{avg}' 
            + '\n' + r'Median coherence $\widetilde{\gamma^2}$ = ' + f'{mean_cohs_med:.2f}' 
            + r',  Bias $b = \rho/\mu$ = ' + f'{mean_bias:.2f}')
s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel('$\Re(\mathrm{CSD})$ [$\mathrm{V}^2/\mathrm{Hz}$]')

s1.set_xlim(ff[0], ff[-1]) 
s1.set_ylim(2e-4, 1e-3) 

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)

s1.legend(loc='lower left')

plt.tight_layout()

plt.show()