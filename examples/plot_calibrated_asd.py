"""
Plots the calibrated DARM ASD.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["H1:SUS-ETMX_M0_DAMP_L_IN1_DQ",
            "H1:SUS-ETMX_M0_DAMP_P_IN1_DQ",
            "H1:SUS-ETMX_M0_DAMP_R_IN1_DQ",
            "H1:SUS-ETMX_M0_DAMP_T_IN1_DQ",
            "H1:SUS-ETMX_M0_DAMP_Y_IN1_DQ",
            "H1:SUS-ETMX_M0_DAMP_V_IN1_DQ"]
gps_start = 1392565137
gps_stop = gps_start + 1000
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo-wa.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.get_psds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

plot_chans = [
    "H1:SUS-ETMX_M0_DAMP_L_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_P_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_R_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_T_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_Y_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_V_IN1_DQ"
]  # only plot DARM, recall that this data dict contains ISS info as well and plot_ASDs will plot it
xlims = [1e-1, 1.3e2]  # set the x-axis limits for the plot
ylims = [1e-5, 1]  # set the y-axis limits for the plot

import matplotlib.pyplot as plt

units = "V"
nu.plot_asds(
    dataDict,
    plot_chans=plot_chans,
    title="Calibrated and Logbinned DARM spectra",
    logbin=False,
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.figure(1)


# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_L_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)

# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_P_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)
# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_R_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)
# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_T_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)
# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_Y_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)
# Make the DARM calibration using zpk
#for n in 1:len(channels)
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
units = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:SUS-ETMX_M0_DAMP_V_IN1_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=units,  
)
##
# Define some plot parameters (Craig)
plot_chans = [
    "H1:SUS-ETMX_M0_DAMP_L_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_P_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_R_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_T_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_Y_IN1_DQ",
    "H1:SUS-ETMX_M0_DAMP_V_IN1_DQ"
]  # only plot DARM, recall that this data dict contains ISS info as well and plot_ASDs will plot it
xlims = [1e-1, 1e2]  # set the x-axis limits for the plot
ylims = [1e-17, 1]  # set the y-axis limits for the plot

nu.plot_asds(
    dataDict,
    plot_chans=plot_chans,
    title="Calibrated and Logbinned DARM spectra",
    logbin=False,
    xlims=xlims,
    ylims=ylims,
    units=units,
)
plt.show()


