#!/bin/bash
for f in *.py; do
    printf "\n"
    echo python $f
    printf "\n"
    python "$f"; 
done
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
printf "${GREEN}"
printf "Example Tests Complete"
printf "${NC}"
