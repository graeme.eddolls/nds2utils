"""find_channels.py

Use wildcards to quickly find and print out available channels on an NDS server.
In this example, we look for all 16384 Hz DQed channels in the PEM subsystem located on h1daqnds0:8088.

nds.ligo-wa.caltech.edu:31200 and nds.ligo-la.caltech.edu:31200 also works.
"""

import nds2utils as nu

# Finds all DQed PEM channels
channel_glob = "H1:PEM-*_DQ"

# Finds DQed PEM channels for EX and EY
channel_glob = "H1:PEM-E?_*_DQ"

# Sets the minimum sample rate, if needed
min_sample_rate = 2**14 # Hz

# Uses the front-end NDS
server = "h1daqnds0"
port = 8088

# Uses permanent NDS2 storage
# server = "nds.ligo-wa.caltech.edu"
# port = 31200

# buffers = nu.find_channels(channel_glob, min_sample_rate=min_sample_rate, host_server=server, port_number=port)
buffers = nu.find_channels(channel_glob, host_server=server, port_number=port)