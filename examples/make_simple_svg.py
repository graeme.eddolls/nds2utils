'''
make_simple_svg.py
Creates an interactive legend .svg plot file from simple matplotlib fig plot.

Craig Cahillane
Nov 23, 2019
'''

import numpy as np
import matplotlib.pyplot as plt
from nds2utils.make_interactive_svg import make_interactive_svg

xx = np.linspace(0.0, 1.0, 50)
yy = xx**(-xx)
yy2 = 0.1*xx**(2)+1
yy3 = 0.3*(xx-0.5)**(3)+1

fig, ss = plt.subplots(1)
ss.plot(xx, yy, lw=3, label='yy')
ss.plot(xx, yy2, lw=3, label='yy2')
ss.plot(xx, yy3, lw=3, label='yy3')

ss.legend()

make_interactive_svg(fig, 'make_simple_svg')
